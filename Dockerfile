FROM openjdk:19 as builder
WORKDIR application
ADD ./target/*.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM openjdk:19

ENV TZ=Asia/Shanghai
ENV JAVA_OPTS='-server -Xms2048m -Xmx2048m'

ENV LANG C.UTF-8
EXPOSE 9191
WORKDIR application
COPY --from=builder application/dependencies/ ./
RUN true
COPY --from=builder application/snapshot-dependencies/ ./
RUN true
COPY --from=builder application/spring-boot-loader/ ./
RUN true
COPY --from=builder application/application/ ./
RUN true
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom ${JAVA_OPTS} org.springframework.boot.loader.JarLauncher"]
